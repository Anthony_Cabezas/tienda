class Carrito{

    comprarProducto(e){
        if(e.target.classList.constains('agregar-carrito')){//click en el boton comprar del arituculo
            const producto = e.target.parentElement.parentElement;
            this.leerDatosProducto(producto);
        }
    }

    leerDatosProducto(producto){
        const infoProducto = {
            imagen : producto.querySelector('img').src,
            titulo : producto.querySelector('.title').textContent,
            precio : producto.querySelector('span').textContent,
            id : producto.querySelector('a').getAttribute('data-id'),
            cantidad : 1
        }
        this.insertarCarrito(infoProducto);

    }

    insertarCarrito(producto){
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>
                <img src="${producto.imagen}" width = 100>
            </td>
            <td>${producto.titulo}</td>
            <td>${producto.precio}</td>
            <td>
                <a href="#" class = "borrar-producto fas fa-times-circle" data-id="${producto.id}"></a>
            </td>
            
        `;
        listaProductos.appendChild(row);

    }

    eliminarProducto(e){
        e.preventDefault();
        let producto, productoID;
        if(e.target.classList.constains('borrar-producto')){
            e.target.parentElement.parentElement.remove();
            producto = target.parentElement.parentElement;
            productoID = producto.querySelector('a').getAttribute('data-id')
        }
    }
}