document.querySelector('#login').addEventListener('click',function(){
    modal_login();
});

document.querySelector('#registro').addEventListener('click',function(){
    modal_registro();
});

document.querySelector('#close-login').addEventListener('click',function(){
    cerrar_login();
});

document.querySelector('#close-registro').addEventListener('click',function(){
    cerrar_registro();
}) ;

function modal_login(){
    var modal = document.getElementsByClassName('modal')[0];
    modal.classList.add('is-active');
}

function modal_registro(){
    var modal = document.getElementsByClassName('modal')[1];
    modal.classList.add('is-active');
}

function cerrar_login(){
    var modal = document.getElementsByClassName('modal')[0];
    modal.classList.remove('is-active');
}

function cerrar_registro(){
    var modal = document.getElementsByClassName('modal')[1];
    modal.classList.remove('is-active');
}